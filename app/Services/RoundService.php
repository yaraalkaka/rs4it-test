<?php

namespace App\Services;

use App\Models\Round;

class RoundService
{
    public function index($contest_id)
    {
        return Round::query()
            ->where('contest_id', '=', $contest_id)
            ->get();
    }

    public function store($userData)
    {
        return Round::query()->create([
            'name' => $userData['name'],
            'sponsor' => $userData['sponsor'],
            'prize_value' => $userData['prize_value'],
            'starting_time' => $userData['starting_time'],
            'contest_id' => $userData['contest_id'],
            'round_date' => $userData['round_date']
        ]);
    }

    public function update($userData, $id)
    {
        $round = Round::query()->find($id);
        Round::query()->find($id)->update([
            'name' => $userData['name'] ?? $round['name'],
            'sponsor' => $userData['sponsor'] ?? $round['sponsor'],
            'prize_value' => $userData['prize_value'] ?? $round['prize_value'],
            'starting_time' => $userData['starting_time'] ?? $round['starting_time'],
            'contest_id' => $userData['contest_id'] ?? $round['contest_id'],
            'round_date' => $userData['round_date'] ?? $round['round_date']

        ]);
        return Round::query()->find($id);
    }

    public function delete($id)
    {
        return Round::query()->find($id)->delete();
    }
}
