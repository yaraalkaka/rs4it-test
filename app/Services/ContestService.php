<?php

namespace App\Services;

use App\Models\Contest;

class ContestService
{
    public function index()
    {
        return Contest::query()->get();
    }

    public function store($userData)
    {
        return Contest::query()->create([
            'name' => $userData['name'],
            'description' => $userData['description'],
            'is_available' => $userData['is_available'],
        ]);
    }

    public function update($userData, $id)
    {
        $contest = Contest::query()->find($id);
        Contest::query()->find($id)->update([
            'name' => $userData['name'] ?? $contest['name'],
            'description' => $userData['description'] ?? $contest['description'],
            'is_available' => $userData['is_available'] ?? $contest['is_available'],
        ]);
        return Contest::query()->find($id);
    }

    public function delete($id)
    {
        return Contest::query()->find($id)->delete();
    }
}
