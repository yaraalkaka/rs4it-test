<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContestUpdateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['string', 'unique:contests,name'],
            'description' => ['string',],
            'is_available' => ['boolean'],
        ];
    }
}
