<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContestStoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:contests,name'],
            'description' => ['required', 'string',],
            'is_available' => ['required', 'boolean'],
        ];
    }
}
