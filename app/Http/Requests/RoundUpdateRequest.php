<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoundUpdateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['string', 'unique:contests,name'],
            'sponsor' => ['string',],
            'starting_time' => ['date_format:H:i:s','after:' . now()->format('H:i:s'),],
            'round_date' => ['date', 'after_or_equal:today',],
            'prize_value' => ['numeric', 'min:0'],
            'contest_id' => ['exists:contests,id']
        ];
    }
}
