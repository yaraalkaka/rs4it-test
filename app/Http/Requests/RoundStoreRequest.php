<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoundStoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:rounds,name'],
            'sponsor' => ['required', 'string',],
            'starting_time' => ['required',
                'date_format:H:i:s',
                'after:' . now()->format('H:i:s'),],
            'round_date' => ['required', 'date', 'after_or_equal:today',],
            'prize_value' => ['required', 'numeric', 'min:0'],
            'contest_id' => ['required', 'exists:contests,id']
        ];
    }
}
