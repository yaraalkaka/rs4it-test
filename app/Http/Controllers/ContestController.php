<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContestStoreRequest;
use App\Http\Requests\ContestUpdateRequest;
use App\Http\Responses\Response;
use App\Services\ContestService;
use Illuminate\Http\JsonResponse;
use Throwable;

class ContestController extends Controller
{
    private ContestService $contestService;

    public function __construct(ContestService $contestService)
    {
        $this->contestService = $contestService;
    }

    public function index(): JsonResponse
    {
        $data = [];
        try {
            $categories = $this->contestService->index();
            $data = $categories;
            $message = 'Contest Indexed Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }

    public function store(ContestStoreRequest $request): JsonResponse
    {
        $data = [];

        try {
            $categories = $this->contestService->store($request->validated());
            $data = $categories;
            $message = 'Contest Stored Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }

    public function update(ContestUpdateRequest $request, $id): JsonResponse
    {
        $data = [];
        try {
            $categories = $this->contestService->update($request->validated(), $id);
            $data = $categories;
            $message = 'Contest Updated Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }

    public function delete($id): JsonResponse
    {
        $data = [];
        try {
            $categories = $this->contestService->delete($id);
            $data = $categories;
            $message = 'Contest Deleted Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }
}
