<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoundStoreRequest;
use App\Http\Requests\RoundUpdateRequest;
use App\Http\Responses\Response;
use App\Models\Round;
use App\Services\RoundService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class RoundController extends Controller
{
    private RoundService $roundService;

    public function __construct(RoundService $roundService)
    {
        $this->roundService = $roundService;
    }

    public function index($contest_id): JsonResponse
    {
        $data = [];
        try {
            $categories = $this->roundService->index($contest_id);
            $data = $categories;
            $message = 'Round Indexed Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }

    public function store(RoundStoreRequest $request): JsonResponse
    {
        $data = [];

        try {
            $categories = $this->roundService->store($request->validated());
            $data = $categories;
            $message = 'Round Stored Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }

    public function update(RoundUpdateRequest $request, $id): JsonResponse
    {
        $data = [];
        try {
            $categories = $this->roundService->update($request->validated(), $id);
            $data = $categories;
            $message = 'Round Updated Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }

    public function delete($id): JsonResponse
    {
        $data = [];
        try {
            $categories = $this->roundService->delete($id);
            $data = $categories;
            $message = 'Round Deleted Successfully';
            return Response::Success($data, $message);

        } catch (Throwable $th) {
            $message = $th->getMessage();
            return Response::Error($data, $message);
        }
    }
}
