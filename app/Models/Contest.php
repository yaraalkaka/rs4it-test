<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Contest extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['availability'];
    protected $hidden = ['is_available'];

    public function rounds(): HasMany
    {
        return $this->hasMany(Round::class);
    }
    public function getAvailabilityAttribute(): string
    {
        return $this['is_available'] ? 'متاح للجميع' : 'لاصحاب التذاكر فقط';
    }
}
