<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('contests')->insert([
            'name' => "كأس السعودية",
            'description' => "مسابقة توقع الفائزين بمسابقة كأس السعودية",
            'is_available' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('contests')->insert([
            'name' => "تحدي الاصدقاء",
            'description' => "مسابقة تحدي الاصدقاء لتوقع الفائز",
            'is_available' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('contests')->insert([
            'name' => "أفضل زي مشترك",
            'description' => "مسابقة أفضل زي ليوم التأسيس",
            'is_available' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
