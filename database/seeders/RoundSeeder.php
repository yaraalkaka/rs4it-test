<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('rounds')->insert([
            'name' => "الشوط الأول",
            'sponsor' => "برعاية شخص يرعى الشوط الأول",
            'prize_value' => 10000,
            'starting_time' => Carbon::createFromTime(5, 36, 0),
            'contest_id' => 1,
            'round_date' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('rounds')->insert([
            'name' => "شوط ثاني",
            'sponsor' => "برعاية راعي الشوط الثاني",
            'prize_value' => 120000,
            'starting_time' => Carbon::createFromTime(1, 40, 0),
            'contest_id' => 1,
            'round_date' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
