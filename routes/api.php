<?php

use App\Http\Controllers\ContestController;
use App\Http\Controllers\RoundController;
use Illuminate\Support\Facades\Route;

Route::prefix('contest')->controller(ContestController::class)->group(function () {
    Route::get('/', 'index')->name('contest.index');
    Route::post('/', 'store')->name('contest.store');
    Route::post('/{id}', 'update')->name('contest.update');
    Route::delete('/{id}', 'delete')->name('contest.delete');
});

Route::prefix('round')->controller(RoundController::class)->group(function () {
    Route::get('/{id}', 'index')->name('contest.index');
    Route::post('/', 'store')->name('contest.store');
    Route::post('/{id}', 'update')->name('contest.update');
    Route::delete('/{id}', 'delete')->name('contest.delete');
});
